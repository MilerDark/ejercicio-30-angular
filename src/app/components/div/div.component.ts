import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-div',
  templateUrl: './div.component.html',
  styleUrls: ['./div.component.css']
})
export class DivComponent implements OnInit {

  tamanio: number = 50
  texto: string = '';
  error: string = 'Alcanzo su maximo tamaño'
  constructor() { }

  ngOnInit(): void {

  }

  aumentar(){
    this.tamanio = this.tamanio + 100;
    if (this.tamanio >= 500){
      this.texto = this.error
    }
  }

  disminuir(){
    this.tamanio = this.tamanio - 5;
    if(this.tamanio === 5){
      this.texto = this.error
    }
  }
}
